'use strict';

eventsApp.controller('EventController',
    function EventController($scope){

        $scope.snippet = '<span style="color:red">hi there</span>';
        $scope.boolValue = false;
        $scope.mystyle = {color:'red'};
        $scope.myclass = "blue";
        $scope.buttonDisabled = true;
        $scope.sortorder = 'name';
        $scope.event = {
            name: 'Angular Boot Camp',
            date: 1359781015626,
            time: '10:30 am',
            location: {
                address: "Google Headquarters",
                city: "Mountain View",
                province: "CA"
            },
            imageUrl: '/img/angularjs-logo.png',
            "sessions": [
                {
                    "id": 1,
                    "name": "Directives Masterclass Introductory",
                    "creatorName": "Bob Smith",
                    "duration": 1,
                    "level": "Advanced",
                    "abstract": "In this session you will learn the ins and outs of directives!",
                    "upVoteCount": 0
                },
                {
                    "id": 2,
                    "name": "Scopes for fun and profit",
                    "creatorName": "John Doe",
                    "duration": 3,
                    "level": "Introductory",
                    "abstract": "This session will take a closer look at scopes.  Learn what they do, how they do it, and how to get them to do it for you.",
                    "upVoteCount": 3
                },
                {
                    "id": 3,
                    "name": "Well Behaved Controllers",
                    "creatorName": "Jane Doe",
                    "duration": 2,
                    "level": "Intermediate",
                    "abstract": "Controllers are the beginning of everything Angular does.  Learn how to craft controllers that will win the respect of your friends and neighbors.",
                    "upVoteCount": 2
                }
            ]
        }

        $scope.upVoteSession = function(session){
            session.upVoteCount++;
        };

        $scope.downVoteSession = function(session){
            session.upVoteCount--;
        }
    }
);